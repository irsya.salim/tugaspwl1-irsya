<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tugas 2</title>
    <link rel="stylesheet" href="{{ asset( 'css/bootstrap.min.css' ) }}">
    <script src="{{ asset( 'js/bootstrap.min.js' ) }}"></script>
</head>
<body>
    <nav>
        <br>
    </nav>
    <div class="container-md mt-4 mb-4">
        <div class="container-fluid mt-4 mb-4 px-5">
            <p class="fs-3 fw-semibold">Form Biodata</p>

            <form action="/tambah" method="post">
                @csrf
                <div class="row mb-3">
                    <div class="col-sm-4">
                        <input type="number" name="NIM" id="NIM" class="form-control" placeholder="NIM" required>
                    </div>
                    <div class="col-sm-8">
                        <input name="Alamat" id="Alamat" class="form-control" placeholder="Alamat" required></input>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-4">
                        <input type="text" name="Nama" id="Nama" class="form-control" placeholder="Nama" required>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="Hobi" id="Hobi" class="form-control" placeholder="Hobi" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>

        <div class="container-fluid mt-4 mb-4 px-5">
            <p class="fs-2 fw-semibold">Daftar Mahasiswa</p>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="col-sm-2">NIM</th>
                        <th class="col-sm-3">Nama</th>
                        <th class="col-sm-4">Alamat</th>
                        <th class="col-sm-3">Hobi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mahasiswas as $mahasiswa)
                    <tr>
                        <td>{{ $mahasiswa->NIM }}</td>
                        <td>{{ $mahasiswa->Nama }}</td>
                        <td>{{ $mahasiswa->Alamat }}</td>
                        <td>{{ $mahasiswa->Hobi }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
