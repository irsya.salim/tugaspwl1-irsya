<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tugas 1</title>
    <style>
        body {
          background-color: linen;
        }

        h1 {
          color: cornflowerblue;
          margin-left: 40px;
        }

        li {
            font-family: "Times New Roman", Times, serif;
            margin: 15px;
        }
        </style>
</head>
<body>
    <h1>Biodata</h1>
    <ul>
        <li>NIM: 215150401111037</li>
        <li>Nama: Irsya Salim Fadhilah</li>
        <li>Alamat: Jl. Ikan Lodan 2A No.41B, RT/RW 5/8, Tunjungsekar, Lowokwaru</li>
        <li>Hobi: Nonton Drakor dan Bola</li>
    </ul>
</body>
</html>
